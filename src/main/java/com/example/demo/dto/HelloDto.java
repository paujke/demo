package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;

@ToString
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class HelloDto {

    @JsonProperty("name")
    private final String name;

    @JsonProperty("description")
    private final String description;

    @JsonProperty("author")
    private final AuthorDto author;

    @JsonCreator
    public HelloDto(
            @JsonProperty("name") String name,
            @JsonProperty("description") String description,
            @JsonProperty("author") AuthorDto author
    ){
        this.name = name;
        this.description = description;
        this.author = author;
    }
}
