package com.example.demo.dto;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Builder;
import lombok.Getter;
import lombok.ToString;


@ToString
@Getter
@Builder
@JsonInclude(JsonInclude.Include.NON_EMPTY)
public class RequestStudentDto {
    @JsonProperty("name")
    private final String name;

    @JsonProperty("age")
    private final String age;

    @JsonProperty("kurs")
    private final String kurs;

    @JsonCreator
    public RequestStudentDto(
            @JsonProperty("name") String name,
            @JsonProperty("age") String age,
            @JsonProperty("kurs") String kurs
    ){
        this.name = name;
        this.age = age;
        this.kurs = kurs;
    }
}
