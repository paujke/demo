package com.example.demo.dto

import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.annotation.JsonProperty


class ResponseDto (
    @get:JsonProperty("book_name")
    val bookName: String = "",

    @get:JsonProperty("author")
    val author: AuthorDto = AuthorDto()
)

class AuthorDto (
    @get:JsonProperty("author_name")
    val authorName: String = "",

    @get:JsonProperty("author_age")
    val authorAge: String = ""
)