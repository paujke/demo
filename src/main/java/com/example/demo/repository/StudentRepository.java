package com.example.demo.repository;

import com.example.demo.entity.Students;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface StudentRepository extends JpaRepository<Students, Integer> {
    Students findStudentsByName(String name);

//    @Query(value = "SELECT Students.name, Students.age from Students where Students.name = :name")
//    Students serachSt(String name);
}
