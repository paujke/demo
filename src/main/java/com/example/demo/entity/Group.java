package com.example.demo.entity;


import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "group_t")
public class Group {
    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "student_generator")
    @SequenceGenerator(name = "student_generator", sequenceName = "SEQ_STUD")
    Integer id;

    String name;

    Integer countPeople;



}
