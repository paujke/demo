package com.example.demo.aspect;

import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

import java.util.Arrays;

@Aspect
@Component
@Slf4j
public class AspectLogging {

    @Around("execution(* com.example.demo.service.*.*(*))")
    public Object logging(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object res = pjp.proceed();
        long endTime = System.currentTimeMillis();
        StringBuilder message = new StringBuilder("Method: ");
        message.append(pjp.getSignature().getName()).append(" | ");
        Arrays.stream(pjp.getArgs()).forEach(arg -> message.append(arg).append(" | "));
        log.info(message.toString());
        log.info("Taken time:" + (endTime - startTime));
        return res;
    }

    @Around("@annotation(com.example.demo.service.Logging)")
    public Object loggingAnnotation(ProceedingJoinPoint pjp) throws Throwable {
        long startTime = System.currentTimeMillis();
        Object res = pjp.proceed();
        long endTime = System.currentTimeMillis();
        StringBuilder message = new StringBuilder("Method: ");
        message.append(pjp.getSignature().getName()).append(" | ");
        Arrays.stream(pjp.getArgs()).forEach(arg -> message.append(arg).append(" | "));
        log.info(message.toString());
        log.info("Taken time:" + (endTime - startTime));
        return res;
    }
}
