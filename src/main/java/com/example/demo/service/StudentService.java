package com.example.demo.service;

import com.example.demo.dto.RequestStudentDto;
import com.example.demo.entity.Students;
import com.example.demo.repository.StudentRepository;
import com.example.demo.service.dto.PersonDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;

    public static void main(String[] args) {
        String textJson = "{\"name\": \"Andrey\", \"age\":\"34\", \"status\":\"driver\"}";
        try {
            ObjectMapper mapper = new ObjectMapper();
            PersonDto person = mapper.readValue(textJson, PersonDto.class);
            System.out.println("person name=" + person.getName() + " age=" + person.getAge());
            System.out.println(person);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Logging
    public void addStudent(RequestStudentDto student) {
        Students students = new Students();
        students.setAge(Integer.parseInt(student.getAge()));
        students.setName(student.getName());
        students.setKurs(student.getKurs());
        studentRepository.save(students);

    }

    @Logging
    public String getStudent(String name) {
        Students students = studentRepository.findStudentsByName(name);
        return students.getAge().toString();
    }
}
