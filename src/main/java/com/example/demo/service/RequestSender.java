package com.example.demo.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;
import java.util.Collections;

public class RequestSender {
    private RestTemplate restTemplate;

    public Object makeRequestAndSend(String url, String payloadData) {
        try {
            String asrOrderBlockResponseDto = this.restTemplate.postForEntity(url, this.buildHttpEntityOrderBlockStatus(payloadData), String.class).getBody();
            ObjectMapper jsonMapper = new ObjectMapper();
            jsonMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            String responseDto = jsonMapper.readValue(asrOrderBlockResponseDto, String.class);
            return responseDto;
        } catch (Exception e) {
            System.out.println("Exception " + e);
        }
        return null;
    }

    private HttpEntity<String> buildHttpEntityOrderBlockStatus(String payloadData) throws Exception {
        ObjectMapper jsonMapper = new ObjectMapper();
        final HttpEntity<String> httpEntity = new HttpEntity<>(
                jsonMapper.writeValueAsString(payloadData),
                getHttpHeaders()
        );
        System.out.println("AsrOrderBlockDto HttpEntity=" + httpEntity);
        return httpEntity;
    }

    private HttpHeaders getHttpHeaders() {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(new MediaType("text", "json", Charsets.UTF_8));
        headers.setAccept(Collections.singletonList(MediaType.TEXT_XML));
        return headers;
    }

}
