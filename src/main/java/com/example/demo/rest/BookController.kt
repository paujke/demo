package com.example.demo.rest

import com.example.demo.dto.AuthorDto
import com.example.demo.dto.HelloDto
import com.example.demo.dto.ResponseDto
import io.swagger.annotations.Api
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RestController

@RestController
@RequestMapping("/book")
@Api
class BookController {

    @GetMapping
    fun getBook(): ResponseDto {
        return ResponseDto(
            bookName = "Колобок",
            AuthorDto(
                authorName = "Носов",
                authorAge = "60"
            )
        )
    }

}