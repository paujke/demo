package com.example.demo.rest;

import com.example.demo.dto.AuthorDto;
import com.example.demo.dto.HelloDto;
import com.example.demo.dto.RequestStudentDto;
import com.example.demo.service.Logging;
import com.example.demo.service.StudentService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;


@RestController
@RequestMapping("/hello")
@Api
public class ApiController {

    @Autowired
    private StudentService service;


    @Logging
    @GetMapping(value = "/name")
    public HelloDto getName(
            @RequestParam("name")
                    String name,
            @RequestParam("description")
                    String description
    ) {
        String age = service.getStudent(name);
        return new HelloDto(
                name,
                description,
                new AuthorDto(name, age));
    }

    @GetMapping(value = "/book")
    public HelloDto getBook() {
        return new HelloDto(
                "Andrey",
                "writer",
                new AuthorDto("Pushkin", "40"));
    }

    @PostMapping
    public String setStudent(
            @RequestBody RequestStudentDto student
    ) {
        service.addStudent(student);

        return "add new student name = " + student.getName();
    }
}
